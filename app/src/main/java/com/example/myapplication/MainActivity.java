package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText etID;
    Button btnSubmit;
    TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etID = findViewById(R.id.etID);
        btnSubmit = findViewById(R.id.btnSubmit);
        tvResult = findViewById(R.id.tvResult);

        tvResult.setVisibility(View.GONE);

        btnSubmit.setOnClickListener(view -> {

            String idNumber = etID.getText().toString();

            int numberOfChirps = Integer.parseInt(idNumber);

            float degreeCelsius = numberOfChirps / 3 + 4;
            String sDegreeCelsius = Float.toString(degreeCelsius);
            String text = getString(R.string.txt_display_result).replace("{}", sDegreeCelsius);
            tvResult.setText(text);
            tvResult.setVisibility(View.VISIBLE);
        });
    }
}